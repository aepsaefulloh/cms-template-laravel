@extends('components.home')


@section('content')
<div class="padding">
    <div class="row">
        <div class="col-xs-12 col-sm-3">
            <div class="box p-a rounded-10 shadow-mod">
                <div class="pull-left m-r">
                    <span class="w-48 rounded bg-yellow">
                        <img src="{{ asset('assets') }}/icon/document-yellow.svg" width="20" alt="" />
                    </span>
                </div>
                <div class="clear">
                    <h4 class="m-0 text-lg _700"><a href>32,912</a></h4>
                    <small class="text-muted">Total Orders</small>
                </div>
            </div>
        </div>
        <div class="col-xs-6 col-sm-3">
            <div class="box p-a rounded-10 shadow-mod">
                <div class="pull-left m-r">
                    <span class="w-48 rounded bg-purple">
                        <img src="{{ asset('assets') }}/icon/work-purple.svg" width="20" alt="" />
                    </span>
                </div>
                <div class="clear">
                    <h4 class="m-0 text-lg _700"><a href>8,345</a></h4>
                    <small class="text-muted">Total Sales</small>
                </div>
            </div>
        </div>
        <div class="col-xs-6 col-sm-3">
            <div class="box p-a rounded-10 shadow-mod">
                <div class="pull-left m-r">
                    <span class="w-48 rounded bg-yellow">
                        <img src="{{ asset('assets') }}/icon/car-red.svg" width="20" alt="" />
                    </span>
                </div>
                <div class="clear">
                    <h4 class="m-0 text-lg _700"><a href>8,345</a></h4>
                    <small class="text-muted">Produk Anda</small>
                </div>
            </div>
        </div>
        <div class="col-xs-6 col-sm-3">
            <div class="box p-a rounded-10 shadow-mod">
                <div class="pull-left m-r">
                    <span class="w-48 rounded bg-yellow">
                        <img src="{{ asset('assets') }}/icon/car-red.svg" width="20" alt="" />
                    </span>
                </div>
                <div class="clear">
                    <h4 class="m-0 text-lg _700"><a href>8,345</a></h4>
                    <small class="text-muted">Pemasukan</small>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="margin">
                <h6 class="m-b-0 _700">Grafik Transaksi</h6>
            </div>
            <div class="box rounded-10 shadow-mod">
                <div class="box-body">
                    <div id="chart"></div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')


<script>
var options = {

    series: [{
        name: "Desktops",
        data: [1000000, 3000000, 2004000, 4000000, 500000, 5500000, 4900000, 430000, 6000000]
    }],
    chart: {
        height: 350,
        type: 'line',
        zoom: {
            enabled: false
        }
    },
    dataLabels: {
        enabled: false
    },
    stroke: {
        curve: 'smooth'
    },

    title: {
        text: 'Grafik Pertahun',
        align: 'left'
    },
    grid: {
        row: {
            colors: ['#f3f3f3', 'transparent'], // takes an array which will be repeated on columns
            opacity: 0.5
        },
    },
    xaxis: {
        categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep'],
    }
};

var chart = new ApexCharts(document.querySelector("#chart"), options);
chart.render();
</script>
@endpush