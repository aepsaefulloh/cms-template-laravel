<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>Administrator</title>
    <meta name="description" content="Praesent auctor varius enim, eget faucibus sapien eleifend eget." />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimal-ui" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- for ios 7 style, multi-resolution icon of 152x152 -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-barstyle" content="blue-white">
    <link rel="apple-touch-icon" href="{{ asset('assets') }}/images/logo.png">
    <meta name="apple-mobile-web-app-title" content="">
    <!-- for Chrome on Android, multi-resolution icon of 196x196 -->
    <meta name="mobile-web-app-capable" content="yes">
    <link rel="shortcut icon" sizes="196x196" href="{{ asset('assets') }}/images/logo.png">

    @include('components.include.script-css')
    @yield('css');
</head>

<body>
    <div class="app" id="app">
        @include('components.include.aside')

        <div id="content" class="app-content box-shadow-z0" role="main">
            @include('components.include.header')
            @include('components.include.footer')
            <div ui-view class="app-body" id="view">
                @yield('content')
            </div>
        </div>
    </div>
    @include('components.include.script-js');
    @stack('scripts')
</body>

</html>