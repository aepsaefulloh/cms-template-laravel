@extends('components.home')

@section('content')
<div class="padding">
    <div class="box rounded-10 shadow-mod">
        <div class="row p-a">
            <div class="col-sm-9">
                <h5>Config</h5>
            </div>
        </div>
        <div class="box-body">
            <form role="form">
                <div class="row row-sm">
                    <div class="col-md-6">
                        <div class="form-wrapper">
                            <div class="form-group form-with-icon relative">
                                <label>Add Menu</label>
                                <div class="input-group m-b">
                                    <input type="text" name="field_name[]" class="form-control" placeholder="Menu 1">
                                    <a href="javascript:void(0)" class="btn btn-icon white add_button"
                                        class="btn btn-icon white" title="Add field">
                                        <i class="fa fa-plus"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn white m-b">Submit</button>
            </form>
        </div>
    </div>
</div>

@endsection
@push('scripts')
<script>
$(document).ready(function() {
    var maxField = 10; //Input fields increment limitation
    var addButton = $('.add_button'); //Add button selector
    var wrapper = $('.form-wrapper'); //Input field wrapper
    var fieldHTML =
        '<div class="input-group m-b"><input type="text" name="field_name[]" class="form-control" value="" placeholder=""/> <a href="javascript:void(0)" class="btn btn-icon white remove_button" class="btn btn-icon white" title="Add field"><i class="fa fa-remove"></i></a></div>'; //New input field html 
    var x = 1; //Initial field counter is 1

    //Once add button is clicked
    $(addButton).click(function() {
        //Check maximum number of input fields
        if (x < maxField) {
            x++; //Increment field counter
            $(wrapper).append(fieldHTML); //Add field html
        }
    });

    //Once remove button is clicked
    $(wrapper).on('click', '.remove_button', function(e) {
        e.preventDefault();
        $(this).parent('div').remove(); //Remove field html
        x--; //Decrement field counter
    });
});
</script>
@endpush