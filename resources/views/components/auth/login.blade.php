@section('content')

@extends('components.home')
<div class="app" id="app">

    <div id="content" class="app-content box-shadow-z0" role="main">
        <div ui-view class="app-body" id="view">
            <div class="center-block w-xxl w-auto-xs p-y-md">
                <div class="navbar">
                    <a class="navbar-brand text-center">
                        <img src="assets/images/logo-long.png" width="80%" alt="LOGO" class="img-fluid" />
                    </a>
                </div>

                <div class="p-a-md box-color r shadow-mod text-color m-a rounded-10">
                    <form method="post">
                        <div class="md-form-group float-label">
                            <input type="text" class="md-input" />
                            <label>Username</label>
                        </div>
                        <div class="md-form-group float-label">
                            <input type="password" class="md-input" />
                            <label>Password</label>
                        </div>
                        <!-- <button type="submit" class="md-btn md-raised indigo btn-block p-x-md">
                        Sign in
                    </button> -->
                        <a href="index.html" class="md-btn md-raised warn btn-block p-x-md">Sign In</a>
                    </form>
                </div>
            </div>
        </div>
    </div>




</div>
@endsection