<div id="aside" class="app-aside modal nav-dropdown">
    <div class="left navside white shadow-aside dk" data-layout="column">
        <div class="navbar no-radius">
            <a href="{{ url('/') }}" class="navbar-brand">
                <img src="assets/images/logo-long.png" alt="logo" width="" class="img-fluid" />
            </a>
        </div>
        <div class="hide-scroll" data-flex>
            <nav class="scroll nav-light">
                <ul class="nav" ui-nav>
                    <li>
                        <a href="{{ url('/') }}">
                            <span class="nav-icon">
                                <img src="assets/icon/black/dashboard.svg" width="20" alt="" />
                            </span>
                            <span class="nav-text">Dashboard</span>
                        </a>
                    </li>

                    <li>
                        <a>
                            <span class="nav-caret">
                                <i class="fa fa-caret-down"></i>
                            </span>
                            <span class="nav-icon">
                                <img src="assets/icon/black/dashboard.svg" width="20" alt="" />
                            </span>
                            <span class="nav-text">Data Master</span>
                        </a>
                        <ul class="nav-sub">
                            <li>
                                <a href="{{ url('/user') }}">
                                    <span class="nav-text">User</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{ url('/role') }}">
                                    <span class="nav-text">Role</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{ url('/category') }}">
                                    <span class="nav-text">Kategori</span>
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li>
                        <a href="{{ url('/content') }}">
                            <span class="nav-icon">
                                <img src="assets/icon/black/dashboard.svg" width="20" alt="" />
                            </span>
                            <span class="nav-text">Content</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('/config') }}">
                            <span class="nav-icon">
                                <img src="assets/icon/black/dashboard.svg" width="20" alt="" />
                            </span>
                            <span class="nav-text">Config</span>
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
</div>