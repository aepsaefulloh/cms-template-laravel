<div class="app-footer">
    <div class="p-2 text-xs">
        <span class="text-white">Aep Saefulloh</span>
        <div class="pull-right text-muted py-1">
            &copy; Copyright <strong>Sketsahouse</strong>
            <span class="hidden-xs-down"></span>
            <a ui-scroll-to="content"><i class="fa fa-long-arrow-up p-x-sm"></i></a>
        </div>
    </div>
</div>