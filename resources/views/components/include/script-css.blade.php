<!-- style -->
<link rel="stylesheet" href="{{ asset('assets') }}/animate.css/animate.min.css" type="text/css" />
<link rel="stylesheet" href="{{ asset('assets') }}/glyphicons/glyphicons.css" type="text/css" />
<link rel="stylesheet" href="{{ asset('assets') }}/font-awesome/css/font-awesome.min.css" type="text/css" />
<link rel="stylesheet" href="{{ asset('assets') }}/material-design-icons/material-design-icons.css" type="text/css" />

<link rel="stylesheet" href="{{ asset('assets') }}/bootstrap/dist/css/bootstrap.min.css" type="text/css" />
<link rel="stylesheet" href="{{ asset('assets') }}/libs/apex_chart/apexcharts.css" type="text/css" />

<link rel="stylesheet" href="{{ asset('assets') }}/styles/app.min.css">
<link rel="stylesheet" href="{{ asset('assets') }}/styles/font.css" type="text/css" />
<link rel="stylesheet" href="{{ asset('assets') }}/styles/custom.css" type="text/css" />