@extends('components.home')

@section('content')
<div class="padding">
    <div class="box rounded-10 shadow-mod">
        <div class="row p-a">
            <div class="col-sm-9">
                <h5>Content</h5>
            </div>
            <div class="col-sm-3">
                <div class="input-group input-group-sm">
                    <input type="text" class="form-control" placeholder="Search" />
                </div>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-striped b-t">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Title</th>
                        <th>Image</th>
                        <th>Author</th>
                        <th>Category</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>#88992021</td>
                        <td class="text-capitalize">
                            pedagang pasar surati Jokowi Soal Minyak Goreng Langka dan Mahal
                        </td>
                        <td>
                            <img src="https://cf.shopee.co.id/file/480c862a0b07599337b266a6b941b0dc" class="img-fluid"
                                width="50" alt="">
                        </td>
                        <td>
                            Jajang Uhuy
                        </td>
                        <td>
                            Article
                        </td>
                        <td>
                            <div>
                                <a href="#" class="label green p-1"> Publish </a>
                            </div>

                        </td>
                        <td>
                            <a href="javascript:void(0)" class="btn btn-icon info btn-sm">
                                <i class="fa fa-pencil-square-o"></i>
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td>#88992021</td>
                        <td class="text-capitalize">
                            Pemerintah merencanakan strategi arus mudik lebaran 2022
                        </td>
                        <td>
                            <img src="https://cdn.antaranews.com/cache/800x533/2013/07/2013070178.jpg" class="img-fluid"
                                width="50" alt="">
                        </td>
                        <td>
                            Ujang Candy
                        </td>
                        <td>
                            Article
                        </td>
                        <td>
                            <div>
                                <a href="#" class="label danger p-1"> Unpublish </a>
                            </div>

                        </td>
                        <td>
                            <a href="javascript:void(0)" class="btn btn-icon info btn-sm">
                                <i class="fa fa-pencil-square-o"></i>
                            </a>
                        </td>
                    </tr>

                </tbody>
            </table>
        </div>
        <footer class="dker p-a">
            <div class="row justify-content-end">
                <ul class="pagination pagination-sm m-a-0">
                    <li>
                        <a href="javascript:void(0)"><i class="fa fa-chevron-left"></i></a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">1</a>
                    </li>
                    <li>
                        <a href="javascript:void(0)"><i class="fa fa-chevron-right"></i></a>
                    </li>
                </ul>
            </div>
        </footer>
    </div>
</div>
@endsection